package br.com.programacao2.cliente;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AtualizarClientes {

	
	public void atualizarClientes(String id, String endereco) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://10.68.103.50:3307/aplicacaojava","jaime.borges","");
		PreparedStatement ps = conn.prepareStatement("update tb_cliente set endereco=? where id_cliente=?");
		ps.setString(1, endereco);
		ps.setInt(2, Integer.parseInt(id));
                
		int res = ps.executeUpdate();
                
		System.out.println(res+" registro foi atualizado!!");
                
		ps.close();
		conn.close();
	}
	
	public static void main(String[] args) throws SQLException {
		AtualizarClientes ac = new AtualizarClientes();
		ac.atualizarClientes("1", "Quadra 1 Conj 1 casa 1");
	}
	
}
