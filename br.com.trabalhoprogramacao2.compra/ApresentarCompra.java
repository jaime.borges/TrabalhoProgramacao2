package br.com.programacao2.compra;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ApresentarCompra{

	public void lerClientes() throws SQLException, ClassNotFoundException {
		//Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://10.68.103.50:3307/aplicacaojava","jaime.borges","");
		if (conn != null) {
			System.out.println("Conectado ao banco de dados!!");
			PreparedStatement ps = conn.prepareStatement("select * from tb_cliente");
			System.out.println("Imprimindo as informações dos clientes!!");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("Código:   "+rs.getInt(1));
				System.out.println("Nome:     "+rs.getString(2));
				System.out.println("Endereço: "+rs.getString(3));
				System.out.println("telefone: "+rs.getInt(4));
				System.out.println("cpf:      "+rs.getInt(5));
			}
			rs.close();
			ps.close();
			conn.close();
		}
	
	}
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		ApresentarCompra lc = new ApresentarCompra();
		lc.lerClientes();
	}
	
}

