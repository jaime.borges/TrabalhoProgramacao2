package br.com.programacao2.produto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExcluirProduto{

	public void excluirClientes(String id) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://10.68.103.50:3307/aplicacaojava","jaime.borges","");
		PreparedStatement ps = conn.prepareStatement("delete from tb_cliente where id_cliente=?");
		ps.setInt(1, Integer.parseInt(id));
		
                int res = ps.executeUpdate();
		
                System.out.println(res+" registro foi excluído");
                
		ps.close();
		conn.close();
	}
	
	public static void main(String[] args) throws SQLException {
		ExcluirProduto ec = new ExcluirProduto();
		ec.excluirClientes("1");
	}
	
}
